package com.ergosign.impl

import com.intellij.ide.actions.CreateFileAction
import com.intellij.openapi.application.WriteAction
import com.intellij.openapi.fileTypes.StdFileTypes
import com.intellij.psi.PsiDirectory
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiFile
import com.intellij.psi.PsiFileFactory
import com.intellij.util.IncorrectOperationException
import org.jetbrains.annotations.Nls

import org.jetbrains.annotations.NotNull

import org.jetbrains.annotations.Nullable


class WebComponent : CreateFileAction("Web Component", "Create new Web Component", null){

    override fun create(newName: String, directory: PsiDirectory?): Array<PsiElement> {

        val mkdirs = MkDirs(newName, directory!!)

        val newDir = mkdirs.directory.createSubdirectory(newName.capitalize());

        val fileFactory = PsiFileFactory.getInstance(newDir.project)

        val reactFile = fileFactory
            .createFileFromText(getFileName(mkdirs.newName) + ".jsx", StdFileTypes.JS, createReactFile(getFileName(mkdirs.newName)))




        return arrayOf(
            WriteAction.compute<PsiDirectory, IncorrectOperationException> {
                newDir
            },
            WriteAction.compute<PsiElement, RuntimeException> {
                newDir.add(reactFile)
            },
            WriteAction.compute<PsiFile, RuntimeException> {
                newDir.createFile(getFileName(mkdirs.newName) + ".scss")
            },
            WriteAction.compute<PsiFile, java.lang.RuntimeException> {
                newDir.createFile(getFileName(mkdirs.newName) + ".test.js")
            }
        )
    }

    fun createReactFile(@NotNull filename: String): String {

        return(
                """
                import React from 'react'
                import './${filename}.scss';
                
                export default ${filename} = () => {
                
                };
                """.trimIndent()
                )

    }
}
